<?php
include "vendor/autoload.php" ;
use App\Ask_question\Question;
$ask = new Question();
$det = $ask->getdata();
// print_r($det);
// die();
session_start();

?>
<?php
          if (isset($_SESSION['message'])) {
            echo "<p class='alert alert-success session'>".$_SESSION['message']."</p>";
            unset($_SESSION['message']);
          }
          ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="icon" href="">

    <title>CodeWell</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/custom.css" rel="stylesheet">

  </head>

  <body>

    
    
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">CodeWell</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <!-- <li><a href="#">Dashboard</a></li> -->
            <li><a href="#">Settings</a></li>
            <li><a href="#">Categories</a></li>
            <li><a href="#">Help</a></li>
            <li><form><a href="pages/login.php"><input type="button" value="Log In" class="navbar-signin"></a></form></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>

    <div class="container wrap">

      <div class="row">
          <div class="col-sm-12 hdr">
              <div>
                  <h1 class="hdr-text">CodeWell.Com</h1>
              </div>
              <div class="ask">
                  <form>
                      <input type="text" name="question" placeholder="Ask a question" class="question"><input type="submit" value="Ask">
                  </form>
              </div>
              <div class="join">
                  <form>
                      <a href="pages/signup.php"><input type="button" Value="Join Our Community" class="join-btn"></a>
                  </form>
              </div>
          </div>

        <div class="col-sm-8 blog-main">

          <div class="blog-post">
            <h3><a href="#">How to use inline CSS?</a></h3>
            <p class="blog-post-meta">January 1, 2014 asked by <a href="#">Mark</a></p>

            <p class="detailed-question">Cum sociis natoque penatibus et magnis <a href="#">dis parturient montes</a>, nascetur ridiculus mus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.<a href="pages/question.php">Read more>></a><br>
            <span class="ctg">CSS</span><span class="ctg">HTML</span><span class="ctg">web design</span></p>
            
          </div><!-- /.blog-post -->

        <?php foreach($det as $all){?>

          <div class="blog-post">
            
            <h3><a href="question.php?id=<?php echo $all['id'];?>"><?php echo $all['title'];?></a></h3>
            <p class="blog-post-meta">December 23, 2013 asked by <a href="#">Jacob</a></p>
            <div class="detailed-question">
           <?php echo $all['details'] ."<span><a href='#'>Read more>></a></span>";?>
            
            <span class="ctg">CSS</span><span class="ctg">HTML</span><span class="ctg">web design</span>
          </div>
          </div><!-- /.blog-post -->
     <?php }?>

          <nav>
            <ul class="pager">
              <li><a href="#">Previous</a></li>
              <li><a href="#">Next</a></li>
            </ul>
          </nav>

        </div><!-- /.blog-main -->

        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
            <div class="sidebar-module sidebar-signup">
                <div class="sidebar-signup-text">
                    <p>Already an user? Log In below</p>
                    
                </div>
                <div class="sidebar-signup-div">
                <form class="sidebar-signup-form" method="post" action="Views/Login/Login_process.php">
                    <input type="email" name="email" placeholder="Email">
                    <input type="password" name="password" placeholder="Password">
                    <input type="submit" Value="Log In" name="signup" class="btn btn-primary btn-sm">
                </form>
                </div>
            </div>
          <div class="sidebar-module">
            <h4 class="category-text">Categories</h4>
            <ol class="list-unstyled">
              <li><a href="#">Linux</a></li>
              <li><a href="#">Windows</a></li>
              <li><a href="#">Python</a></li>
              <li><a href="#">PHP</a></li>
              <li><a href="#">Ruby</a></li>
              <li><a href="#">C++</a></li>
              <li><a href="#">Node.js</a></li>
              <li><a href="#">C#</a></li>
              <li><a href="#">Javascript</a></li>
              <li><a href="#">HTML</a></li>
              <li><a href="#">Wordpress</a></li>
            </ol>
          </div>
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->

    <footer class="blog-footer ftr">
      <p>Web App Development by PHP.</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </footer>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>

  </body>
</html>
