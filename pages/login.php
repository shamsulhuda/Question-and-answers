<?php
          session_start();
          if (isset($_SESSION['message'])) {
            echo "<p class='alert alert-success session'>".$_SESSION['message']."</p>";
            unset($_SESSION['message']);
            
          }
          ?>
          <?php if(!empty($_SESSION['user_id'])){
            	header('location:home.php');
            }
            ?>
<!DOCTYPE html>
<html>
  <head>
    <title>Codewell login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="../css/login.css" rel="stylesheet">
    <style>
    	.text-left{
    		font-weight: bold;
    	}
    </style>

  </head>
  <body class="login-bg">
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <!-- Logo -->
	              <div class="logo admin">
	                 <h1><a href="index.php">CodeWell.com</a></h1>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>


	<div class="page-content container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
			        <div class="box">
			            <div class="content-wrap">
			                <h6>Log In</h6>
			                <form method="post" action="../Views/Login/Login_process.php">
			                <div class="form-group">
			                <p class="text-left">E-mail</p>
			                <input class="form-control" type="email" placeholder="E-mail address" name="email">
			                <p class="text-left">Password</p>
			                <input class="form-control" type="password" placeholder="Password" name="password">
			                <div class="action">
			                    <input type="submit" class="btn btn-primary signup" value="Log In">
			                </div>
			                </div>
			                </form>           
			            </div>
			        </div>
			        <div>Not an user? <a href="signup.php">Sign Up here</a></div>

			    </div>
			</div>
		</div>
	</div>
	<!-- <footer class="blog-footer ftr">
      <p>Blog template built for <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </footer> -->



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
  </body>
</html>