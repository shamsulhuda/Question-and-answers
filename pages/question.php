<?php
include "../vendor/autoload.php" ;
use App\Ask_question\Question;
$ask = new Question();
$det = $ask->showQuestion($_GET['id']);
$ans_data = $ask->showAnswer($_GET['id']);
if(!isset($_GET['id'])){
  header('location:home.php');
}
// print_r($ans_data);
// die();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="icon" href="">

    <title>CodeWell</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/custom.css" rel="stylesheet">
      <!-- <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
      <script>tinymce.init({ selector:'textarea' });</script> -->
      <script src="../js/ckeditor/ckeditor.js"></script>

  </head>

  <body>

    
    
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">CodeWell</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Help</a></li>
            
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>

    <div class="container wrap">

      <div class="row">
          <div class="col-sm-12 hdr">
              <div>
                  <h1 class="hdr-text">CodeWell.Com</h1>
              </div>
              
            
          </div>

        <div class="col-sm-8 blog-main">
          <div class="blog-post">
            <h3><?php echo $det['title'];?></h3>
            <p class="blog-post-meta">January 1, 2014 asked by <a href="#">Mark</a></p>
            <div  class="detailed-question">
            <p><?php echo $det['details']; ?>
            <span class="ctg">CSS</span><span class="ctg">HTML</span><span class="ctg">web design</span></p></div>

            </div>
            <h3 class="ans-begin">User answers</h3><hr class="hr">
            <?php foreach ($ans_data as $ans){?>
          <div class="answer">
              <div class="ans-img">
               <img class="img" src="../image/avatar.png">
              </div>
            <div class="ans-det">
              <p class="ans-meta">Brad>> 4th aug, 20017</p>
              
              <p class="main-ans"><?php echo $ans['details']; ?></p>
              
            </div>
          </div>
          <?php }?>
          <div class="clear"></div>
            <hr class="hr">
            <h3>Add an answer</h3>
           <form method="post" action="../Views/Give_ans/Give_ans.php">
           <div class="form-group">
           <textarea name="ans"></textarea>
           <script>
             CKEDITOR.replace( 'ans' );
            </script>
            <input type="hidden" value="<?php echo $_GET['id'];?>" name="question_id">
           <input type="submit" value="Submit">
           </div>

           </form>
            
        </div><!-- /.blog-main -->

        <div class="col-sm-4 blog-sidebar">
            <div class="sidebar-module sidebar-signup tips">
                <h2>Tips</h2>
                <ul>
                  <li>
                    Make your answer precise.
                  </li>
                  <li>
                    If possible use reference(s).
                  </li>
                  <li>
                    Avoid duplicate Answer.
                  </li>
                  <li>
                    Respect our policy.
                  </li>
                </ul>
      
            </div>
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->

    <footer class="blog-footer ftr">
      <p>Web App Development by PHP.</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </footer>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="../js/bootstrap.min.js"></script>

  </body>
</html>
