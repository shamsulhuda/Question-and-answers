
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="icon" href="">

    <title>Ask a question</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/custom.css" rel="stylesheet">
    <!-- <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script> -->
    <!-- <script src="https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script> -->
    <script src="../js/ckeditor/ckeditor.js"></script>

  </head>

  <body>

    
    
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">CodeWell</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Help</a></li>
            
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>

    <div class="container wrap">

      <div class="row ask-question">

        <div class="col-sm-8 blog-main">

          <div class="blog-post">
          <form method="post" action="../Views/Ask_question/Ask.php">
              <div class="form-group">
             Title: <input type="text" class="form-control" placeholder="Write Your Question. Be Precise!" name="title"><hr class="hr">
             <span class="ltl-det">Add little details for convenient:</span> <textarea  name="details"></textarea>
             <script>
             CKEDITOR.replace( 'details' );
            </script>
              <hr class="hr"><hr class="hr">
             <span class="ltl-det">Tags: </span><input type="text" class="form-control" placeholder="tags.e.g; HTML,CSS,PHP,C++"><br>
             <input type="submit" value="Ask to community" class="btn btn-success btn-block">
              
             </div>
            </form>
          </div><!-- /.blog-post -->


        </div><!-- /.blog-main -->

        <div class="col-sm-4 blog-sidebar">
        <div class="sidebar-module sidebar-signup tips">
                <h2>Tips</h2>
                <ul>
                  <li>
                    Ask relevent questions.
                  </li>
                  <li>
                    Be to the point.
                  </li>
                  <li>
                    Try to add some details.
                  </li>
                  <li>
                    Respect our policy.
                  </li>
                </ul>
      
            </div>

        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->

    <footer class="blog-footer ftr">
      <p>Web App Development by PHP.</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </footer>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="../js/bootstrap.min.js"></script>

  </body>
</html>
