<!DOCTYPE html>
<html>
  <head>
    <title>CodeWell</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="../css/login.css" rel="stylesheet">
    <style>
        .text-left{
            font-weight: bold;
        }
    </style>

  </head>
  <body class="login-bg">
    <div class="header">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <!-- Logo -->
                  <div class="logo admin">
                     <h1><a href="index.php">CodeWell.com</a></h1>
                  </div>
               </div>
            </div>
         </div>
    </div>

    <div class="page-content container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-wrapper">
                    <div class="box">
                        <div class="content-wrap">
                            <h6>Sign Up</h6>
                            <p>or already an user? <a href="login.php">Sign in here</a></p>
                            <form method="post" action="../Views/Auth/Store.php">
                            <div class="form-group">
                            <p class="text-left">First Name</p>
                            <input class="form-control" type="text" placeholder="Enter Your First Name" name="firstname" required>
                            <p class="text-left">Last Name</p>
                            <input class="form-control" type="text" placeholder="Enter Your Last Name" name="lastname" required>
                            <p class="text-left">E-mail</p>
                            <input class="form-control" type="email" placeholder="Enter an E-mail address" name="email" required>
                            <p class="text-left">Password</p>
                            <input class="form-control" type="password" placeholder="Enter a Password" name="password" required>
                            <p class="text-left">Confirm Password</p>
                            <input class="form-control" type="password" placeholder="Confirm Password" required>
                            <p class="text-left">User Name</p>
                            <input class="form-control" type="text" placeholder="Enter an User Name" name="username" required>
                            <p class="text-left">Gender <label class="radio-inline">
                              <input type="radio" name="gender" id="inlineRadio1" value="male" checked> Male
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="gender" id="inlineRadio2" value="female"> Female
                            </label></p>
                            
                            <div class="action">
                                <input class="btn btn-primary signup" value="Create Account" type="submit">
                            </div>
                            </div>
                            </form>              
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- <footer class="blog-footer ftr">
      <p>Blog template built for <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </footer> -->


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="../js/bootstrap.min.js"></script>
</body>
</html>