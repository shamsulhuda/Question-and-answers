<?php
namespace App\Auth;
use PDO;
session_start();
class Auth{
	private $username;
	private $password;
	private $email;
	private $firstname;
	private $lastname;
	private $gender;
	private $dbuser = 'root';
	private $dbpass = '';

	public function setdata($data=''){
		$this->username = $data['username'];
		$this->password = $data['password'];
		$this->email = $data['email'];
		$this->firstname = $data['firstname'];
		$this->lastname = $data['lastname'];
		$this->gender = $data['gender'];
		return $this;

	}

	public function connect(){

		try {
		$pdo = new PDO('mysql:host=localhost;dbname=codewell', $this->dbuser,$this->dbpass);
		$query = "INSERT INTO users(id, email, password, firstname, lastname, username, gender, created_at) VALUES(:id, :email, :pass, :fn, :ln, :un, :gen, :t)";
		$stmt = $pdo->prepare($query);
		$data = array(':id'=>null,':email'=>$this->email,':pass'=>$this->password,':fn'=>$this->firstname,':ln'=>$this->lastname,':un'=>$this->username,':gen'=>$this->gender, ':t'=>date('Y-m-d h:m:s'));
			$status = $stmt->execute($data);
			if ($status) {
				$_SESSION['msg'] = '<strong>Congratulation!</strong> You Have Successfully Signed Up.';
				header('location:../../pages/home.php');
			}

	} catch (PDOException $e){

		echo 'Error '. $e->getMessage();
	}



	}
	public function login($data=''){
		$pdo = new PDO('mysql:host=localhost;dbname=codewell', $this->dbuser,$this->dbpass);
		$password = "'".$data['password']."'";
		$email = "'".$data['email']."'";
		$query = "SELECT * FROM `users` WHERE `email` = $email AND `password` = $password";
		$stmt = $pdo->prepare($query);
		$stmt->execute();
		$data = $stmt->fetch();
		if(!empty($data)){
			$_SESSION['user_id'] = $data;
			$_SESSION['message'] = "Hello ".$_SESSION['user_id']['username'];
			header('location:../../pages/home.php');
		}else {
			$_SESSION['message'] = "Invalid Password or Email";
			header('location:../../pages/login.php');
		}
		// print_r($data);
		return $data;
	}
	public function logout(){
		if (!empty($_SESSION['user_id'])){

			unset($_SESSION['user_id']);
			$_SESSION['message'] = "Successfully logged out";
			header('location:../../index.php');



		}
	}
} 
