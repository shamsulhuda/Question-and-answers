<?php
namespace App\Ask_question;
use PDO;
class Question{
	private $title;
	private $ans_details;
	private $qid;
	private $details;
	private $dbuser = 'root';
	private $dbpass = '';

	public function setdata($data=''){
		$this->title = $data['title'];
		$this->details = $data['details'];
		return $this;

	}
	public function setDataForAns($data=''){
		$this->ans_details = $data['ans'];
		$this->qid = $data['question_id'];
		return $this;

	}
	public function getdata(){

		$pdo = new PDO('mysql:host=localhost;dbname=codewell', $this->dbuser,$this->dbpass);
		$query = "SELECT * FROM question ORDER BY id DESC";
		$stmt = $pdo->prepare($query);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
		// print_r($data);

}
	public function showQuestion($id=""){
		$pdo = new PDO('mysql:host=localhost;dbname=codewell', $this->dbuser,$this->dbpass);
		$query = "SELECT * FROM question WHERE id=$id";
		$stmt = $pdo->prepare($query);
		$stmt->execute();
		$data = $stmt->fetch();
		return $data;


	}
	public function giveAnswer(){
		$pdo = new PDO('mysql:host=localhost;dbname=codewell', $this->dbuser,$this->dbpass);
		$query = "INSERT INTO answer(id, details, question_id) VALUES(:id, :det, :qid)";
		$stmt = $pdo->prepare($query);
		$data = array(':id'=>null,':det'=>$this->ans_details, ':qid'=>$this->qid);
		$status = $stmt->execute($data);
		if ($status) {
			header("location:../../pages/question.php?id=".$this->qid);
		}

	}
	public function showAnswer($id=""){
		$pdo = new PDO('mysql:host=localhost;dbname=codewell', $this->dbuser,$this->dbpass);
		$query = "SELECT details FROM answer WHERE question_id = $id";
		$stmt = $pdo->prepare($query);
		$stmt->execute();
		$data = $stmt->fetchAll();
		// if($data){
			
		// 	header('location:../../pages/question.php');
		// }
		return $data;
		

	}

	public function ask(){

		try {
		$pdo = new PDO('mysql:host=localhost;dbname=codewell', $this->dbuser,$this->dbpass);
		$query = "INSERT INTO question(id, title, details) VALUES(:id, :title, :det)";
		$stmt = $pdo->prepare($query);
		$data = array(':id'=>null,':title'=>$this->title, ':det'=>$this->details);
		$status = $stmt->execute($data);
		if($status){
			header ('location:../../pages/home.php');
		}
	} catch (PDOException $e){

		echo 'Error '. $e->getMessage();
	}



	}
} 

